tool
extends Control

export var day: int
var is_focused: bool = false

onready var camera = get_parent().get_parent().get_node("camera")

func _process(_delta: float) -> void:
	var d = OS.get_date()
	if d.month == 12 and d.day >= day:
		$sprite.visible = true

func _on_closed_pressed() -> void:
	if is_focused:
		camera.defocus()
	else:
		camera.focus(self)
