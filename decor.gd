extends Node2D

func _ready():
	visible = false

func _process(_delta: float) -> void:
	set_self()

func set_self():
	var d = OS.get_date()
	if d.month != 12:
		visible = false
		return

	if d.day >= 25:
		var door = get_parent().get_node("windows").get_node("25")
		position = door.rect_position + Vector2(6, -30)
		$talo_decor.visible = false
	else:
		var node = get_parent().get_node("windows").get_node(str(d.day))
		position = node.rect_position
		$talo_decor.visible = true
	visible = true
