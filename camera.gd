extends Camera2D

var START: Vector2 = Vector2(ProjectSettings.get_setting("display/window/size/width"), ProjectSettings.get_setting("display/window/size/height")) / 2
const SPEED: float = 2.0

var focused: Control

func _init() -> void:
	limit_right = ProjectSettings.get_setting("display/window/size/width")
	limit_bottom = ProjectSettings.get_setting("display/window/size/height")

func _ready() -> void:
	var err = $Tween.connect("tween_all_completed", self, "set_focus")
	if err != OK:
		print(err)

func focus(node: Control) -> void:
	if node == focused:
		return
	var speed = SPEED
	if focused != null:
		speed = speed / 2
		focused.is_focused = false
	var p = node.rect_position + node.rect_size / 2
	focused = node
	$Tween.remove_all()
	$Tween.interpolate_property(self, "zoom", zoom, Vector2.ONE * 0.2, speed,  Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.interpolate_property(self, "position", position, p, speed, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()

func defocus() -> void:
	if focused == null:
		return

	focused.is_focused = false
	focused = null
	$Tween.remove_all()
	$Tween.interpolate_property(self, "zoom", zoom, Vector2.ONE, SPEED, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.interpolate_property(self, "position", position, START, SPEED, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()

func _unhandled_input(event):
	if event is InputEventMouseButton:
		defocus()


func set_focus():
	if focused != null:
		focused.is_focused = true

