extends Control

export var day: int
export var text: String setget set_text, get_text
onready var camera = get_parent().get_parent().get_node("camera")

var is_focused: bool = false
var can_be_openned: bool = false

func _ready() -> void:
	$opened.visible = false

func _process(_delta: float) -> void:
	var d = OS.get_date()
	if d.month != 12:
		$opened.visible = false
		return
	if d.day == day:
		can_be_openned = true
	elif d.day > day:
		$opened.visible = true
	else:
		$opened.visible = false

func _on_closed_pressed() -> void:
	if not is_focused:
		camera.focus(self)
		return

	if can_be_openned and not $opened.visible:
		$opened.visible = true
	else:
		camera.defocus()

func set_text(t: String):
	$opened.text = t

func get_text() -> String:
	return $opened.text
